# stylectrode

Styles, themes, icons and fonts for electrode and other libraries and apps.

For the full documentation, installation instructions... check [stylectrode documentation page](https://squeak.eauchat.org/libs/stylectrode/).
