
This package intends to provide a few basic styles, themes, icons and fonts for webapps.
It is mainly designed for usage with [the electrode library](https://squeak.eauchat.org/libs/electrode/), but it can be used in any application that can interpret [less styling](http://lesscss.org/).

If you're working on an electrode app, stylectrode base styles are already included in electrode global styles. Install stylectrode in your app only if you want to use custom themes, fonts...
