# List of methods

## Styles

The `index.less` stylesheet contains convenience methods, they are listed below.
It also imports icomoon (to display icons) and a few fonts.

### Imported fonts

By default, the following fonts are included when you import stylectrode:

- Lato
  - Lato
  - Lato-Hairline
  - Lato-Light
  - Lato-Bold
  - Lato-Black
- ZillaSlab
  - ZillaSlab
  - ZillaSlab-Light
  - ZillaSlab-Bold
  - ZillaSlabHighlight
  - ZillaSlabHighlight-Bold

You can also load additional fonts with the followin shorthands:

  - `stylectrode/fonts/CrimsonText.less`
  - `stylectrode/fonts/Lato.less`
  - `stylectrode/fonts/LiberationMono.less`
  - `stylectrode/fonts/SourceSansPro.less`
  - `stylectrode/fonts/ZillaSlab.less`

### Icomoon

Icomoon helps you to display nice icons that can be styled easily.
To use any icomoon icon, you just need to apply the class `icon-<iconName>` to the element you want to have the icon in.
For example `<span class="icon-pencil"></span>` will display a span containing a pencil icon.

The list of all icons supported by stylectrode's icoomoon is available [here](https://squeak.eauchat.org/stylectrode/icomoon/demo.html).

### Foreground coloring methods

Those methods apply the following colors to the `color` style attribute.

**Examples:**

`.front.base` is a shorthand for
```less
{
  color: @base;
}
```

`.front.primaryAndHover()` is a shorthand for
```less
{
  color: @primary;
  &:hover {
    color: @hover;
  }
}
```

| Method name | Applied color |
|:------:|:-----------:|
|`.front.base`| `@base` |
|`.front.hover`| `@hover` |
|`.front.selected`| `@selected` |
|`.front.primary`| `@primary` |
|`.front.secondary`| `@secondary` |
|`.front.success`| `@success` |
|`.front.warning`| `@warning` |
|`.front.error`| `@error` |
|`.front.baseAndHover`| `@base` (or `@hover` when element is hovered) |
|`.front.primaryAndHover`| `@primary` (or `@hover` when element is hovered) |
|`.front.secondaryAndHover`| `@secondary` (or `@hover` when element is hovered) |
|`.front.selectedAndHover`| `@selected` (or `@hover` when element is hovered) |
|`.front.successAndHover`| `@success` (or `@hover` when element is hovered) |
|`.front.warningAndHover`| `@warning` (or `@hover` when element is hovered) |
|`.front.errorAndHover`| `@error` (or `@hover` when element is hovered) |


### Background coloring methods

Those methods apply the following colors to `background-color`, and their contrast to `color`.

**Examples:**

`.back.base` is a shorthand for
```less
{
  background-color: @base;
  color: @baseContrast;
}
```

`.back.primaryAndHover` is a shorthand for
```less
{
  background-color: @primary;
  color: @primaryContrast;
  &:hover {
    background-color: @hover;
    color: @hoverContrast;
  }
}
```

| Method name | Applied color |
|:------:|:-----------:|
|`.back.base`| `@base` and `@baseContrast` |
|`.back.hover`| `@hover` and `@hoverContrast` |
|`.back.selected`| `@selected` and `@selectedContrast` |
|`.back.primary`| `@primary` and `@primaryContrast` |
|`.back.secondary`| `@secondary` and `@secondaryContrast` |
|`.back.success`| `@success` and `@successContrast` |
|`.back.warning`| `@warning` and `@warningContrast` |
|`.back.error`| `@error` and `@errorContrast` |
|`.back.baseAndHover`| `@base` and `@baseContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.primaryAndHover`| `@primary` and `@primaryContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.secondaryAndHover`| `@secondary` and `@secondaryContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.selectedAndHover`| `@selected` and `@selectedContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.successAndHover`| `@success` and `@successContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.warningAndHover`| `@warning` and `@warningContrast` (or `@hover` and `@hoverContrast` when element is hovered) |
|`.back.errorAndHover`| `@error` and `@errorContrast` (or `@hover` and `@hoverContrast` when element is hovered) |

### Animations

| Method name | Description |
|:------:|:-----------:|
|`.animate-spin`| make the element rotate on itself |
|`.animate-color`| make the element change color (white, cyan, green, yellow, white) |
|`.fade-in` `.fade-in-fast`| fade the element in |
|`.fade-out` `.fade-out-fast`| fade the element out |

### Others

| Method name | Description |
|:------:|:-----------:|
|`.hidden`| hide the element (using `display: none !important`) |
|`.clickable`| apply `cursor: pointer` to element |
|`.flexautocenter()`| make the element a `flex` and center vertically and horizontally it's children |
|`.fullscreen()`| make the element positioned with width, height 100%, top and left 0 |
|`.error-text` `.warning-text` `.success-text`| color this element text with `@error`, `@warning` or `@success` color |

## Themes

Themes just provide default variables values.
You may import some stylectrode themes or just create your own one in your app.

Commonly useful variables are:

- @base
- @baseTransparent
- @baseTransparentMore
- @baseTransparentLess
- @baseContrast
- @baseContrastTransparent
- —
- @primary
- @primaryTransparent
- @primaryTransparentMore
- @primaryTransparentLess
- @primaryContrast
- —
- @secondary
- @secondaryContrast
- —
- @hover
- @hoverContrast
- —
- @selected
- @selectedTransparent
- @selectedTransparentMore
- @selectedTransparentLess
- @selectedContrast
- —
- @success
- @successContrast
- @warning
- @warningContrast
- @error
- @errorContrast
- @danger
- @dangerContrast
- —
- @basefont
- @basefontLight
- @basefontBold
- —
- @scrollBar
