# Usage

Import any desired file in the `less` stylesheet of your page.

## Styles

Import default stylesheets with:
```less
@import "stylectrode/index.less";
```
Then use some of it's methods for example like this:
```less
#some-element {
  .base.primaryAndHover();
  .animate-spin();
}
```

## Themes

Import themes with:
```less
@import "stylectrode/themes/<themeName>.less";
```
For example for the default theme:
```less
@import "stylectrode/themes/default.less";
```

You can then use the themes variables like this:
```less
#some-element {
  background-color: @base;
}
```
